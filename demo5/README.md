## Demo deploying from S2I
--------------------------

### Build and deploy application

  Create project
  ```
  $ oc new-project demo5-project
  ```
  Create App
  ```
  $ oc new-app https://gitlab.com/nicknarcise/ocp-build-demos.git --context-dir=demo5 --name=demo5-app
  ```

  Check the objects created
  ```
  $ oc get all --selector app=demo5-app -o name
  ```

  Expose Service
  ```
  $ oc expose service demo5-app
  ```

  Get Route and open it in a browser
  ```
  $ oc get route demo5-app
  ```

  Now let's do a change in the code
  ```
  $ vi src/data/levels
  
  Change parameters in Level 1
    
  Save and quit
  ```
  
  Build Again
  ```
  $ oc start-build demo5-app --follow
  ```
  
  Refresh the browser

### Clean application

  ```
  $ oc delete all --selector app=demo5-app -o name
  $ oc delete project demo5-project
  ```
