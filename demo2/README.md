## Demo deploying from S2I
--------------------------

### Build and deploy application

  Define ENV
  ```
  $ export APP=my-s2i-app
  $ export DIR=app
  $ export PROJECT=demo2-project
  ```
  Create project
  ```
  $ oc new-project $PROJECT
  ```
  Create App
  ```
  $ oc new-app $DIR --name=$APP
  ```

  Check the objects created
  ```
  $ oc get all --selector app=$APP -o name
  ```

  Navigate the same in the Openshift Console

  Build App
  ```
  $ oc start-build $APP --from-dir $DIR --follow
  ```

  Check the again objects created
  ```
  $ oc get all --selector app=$APP -o name
  ```
  Expose Service
  ```
  $ oc expose service $APP
  ```

  Get Route and open it in a browser
  ```
  $ oc get route $APP
  ```

  Now let's do a change in the code
  ```
  $ vi $DIR/views/index.html
  
  Execute command
  :%s/Welcome to your Node.js application on OpenShift/Welcome to your Node.js application on OpenShift V2/g
  
  Save and quit
  ```
  
  Build Again
  ```
  $ oc start-build $APP --from-dir $DIR --follow
  ```
  
  Refresh the browser


### Clean application

  ```
  $ oc delete all --selector app=$APP -o name
  $ oc delete project $PROJECT
  ```
